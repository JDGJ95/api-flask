from flask import Flask, jsonify, request
from flask_mysqldb import MySQL

app = Flask(__name__)

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'sepomex'

connectionDB = MySQL(app)

@app.route('/new/estado', methods = ['POST'])
def postEdo():
    try:
        cursor = connectionDB.connection.cursor()
        insertEdo = "Insert Into estados (d_estado, c_estado) VALUE ('" + request.args.get('estado') + "','" + str(request.args.get('codigo')) + "')"
        cursor.execute(insertEdo)
        connectionDB.connection.commit()
        print('Insertado')
        return jsonify({"Messange": "Insertdao"})
    except:
        return jsonify({"Messange": "Internal Server Error", "status":500})



@app.route('/estados', methods=['GET'])
def getEdos():
    cursor = connectionDB.connection.cursor()
    queryEdo = "Select * From estados"
    cursor.execute(queryEdo)
    estadoRequest = cursor.fetchall()
    estadosReturn = []
    for value in estadoRequest:
        auxDetails = { "id":value[0], "d_estado":value[1], "c_estado":value[2]}
        estadosReturn.append(auxDetails)
        response = jsonify({"Estados":estadosReturn})
        response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/estados/<string:name>', methods=['GET'])
def getEdo(name):
    cursor = connectionDB.connection.cursor()
    queryEdo = "Select * From estados Where d_estado = '" + name + "'"
    cursor.execute(queryEdo)
    estadoRequest = cursor.fetchall()
    estadosReturn = []
    for value in estadoRequest:
        auxDetails = { "id":value[0], "d_estado":value[1], "c_estado":value[2]}
        estadosReturn.append(auxDetails)
    if len(estadosReturn) > 0 :
        response = jsonify({"Estados":estadosReturn})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
    else:
        return jsonify({"Messange": "not found", "status":404})


@app.route('/new/municipio', methods = ['POST'])
def postMnpio():
    try:
        cursor = connectionDB.connection.cursor()
        insertMnpio = "Insert Into municipios (D_municipio, d_ciudad, id_estado) VALUE ('" + request.args.get('municipio') + "','" + request.args.get('codigo') + "' ," + str(request.args.get('idEdo')) +")"
        cursor.execute(insertMnpio)
        connectionDB.connection.commit()
        print('Insertado')
        return jsonify({"Messange": "Insertdao"})
    except:
        return jsonify({"Messange": "Internal Server Error", "status":500})



@app.route('/municipios', methods=['GET'])
def getMnpios():
    cursor = connectionDB.connection.cursor()
    queryMnpio = "Select * From municipios"
    cursor.execute(queryMnpio)
    municipiosRequest = cursor.fetchall()
    municipiosReturn = []
    for value in municipiosRequest:
        auxDetails = { "id":value[0], "D_emunicipio":value[1], "d_ciudad":value[2], "id_estado":value[3]}
        municipiosReturn.append(auxDetails)

    if len(municipiosReturn) > 0 :
        response = jsonify({"Municipios":municipiosReturn})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
    else:
        response = jsonify({"Messange": "not found", "status":404})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
    


@app.route('/municipios/<string:name>', methods=['GET'])
def getMnpio(name):
    cursor = connectionDB.connection.cursor()
    queryMnpio = "Select * From municipios Where D_municipio = '" + name + "'"
    cursor.execute(queryMnpio)
    municipiosRequest = cursor.fetchall()
    municipiosReturn = []
    for value in municipiosRequest:
        auxDetails = { "id":value[0], "D_municipio":value[1], "d_ciudad":value[2], "id_estado":value[3]}
        municipiosReturn.append(auxDetails)

    print(municipiosReturn)
    response = jsonify({"Municipios":municipiosReturn})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/new/colonia', methods = ['POST'])
def postCol():
    try:
        cursor = connectionDB.connection.cursor()
        insertCol = "Insert Into colonias (d_codigo, d_asentamiento, d_cp, d_zona, id_municipio) VALUE ('" + str(request.args.get('cp')) + "','" + request.args.get('colonia')+ "','" + request.args.get('cp')+ "','" + request.args.get('zona')+ "' ," + str(request.args.get('idMnpio')) +")"
        cursor.execute(insertCol)
        connectionDB.connection.commit()
        print('Insertado')
        return jsonify({"Messange": "Insertdao"})
    except:
        return jsonify({"Messange": "Internal Server Error", "status":500})


@app.route('/colonias', methods=['GET'])
def getCols():
    cursor = connectionDB.connection.cursor()
    queryCol = "Select * From colonias"
    cursor.execute(queryCol)
    coloniasRequest = cursor.fetchall()
    coloniasReturn = []
    for value in coloniasRequest:
        auxDetails = { "id":value[0], "d_codigo":value[1], "d_asentamiento":value[2], "d_cp":value[3], "d_zona":value[4], "id_municipio":value[5]}
        coloniasReturn.append(auxDetails)

    response = jsonify({"Colonias":coloniasReturn})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/colonias/<string:name>', methods=['GET'])
def getCol(name):
    cursor = connectionDB.connection.cursor()
    queryCol = "Select * From colonias Where d_asentamiento = '" + name +"'"
    cursor.execute(queryCol)
    coloniasRequest = cursor.fetchall()
    coloniasReturn = []
    for value in coloniasRequest:
        auxDetails = { "id":value[0], "d_codigo":value[1], "d_asentamiento":value[2], "d_cp":value[3], "d_zona":value[4], "id_municipio":value[5]}
        coloniasReturn.append(auxDetails)
    
    if len(coloniasReturn) > 0 :
        response = jsonify({"Colonias":coloniasReturn})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
    else:
        response = jsonify({"Colonias":coloniasReturn})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response


@app.route('/colonias/CP/<int:cp>', methods=['GET'])
def getColCp(cp):
    cursor = connectionDB.connection.cursor()
    queryCol = "Select * From colonias Where d_codigo = '" + str(cp) +"'"
    cursor.execute(queryCol)
    coloniasRequest = cursor.fetchall()
    coloniasReturn = []
    for value in coloniasRequest:
        auxDetails = { "id":value[0], "d_codigo":value[1], "d_asentamiento":value[2], "d_cp":value[3], "d_zona":value[4], "id_municipio":value[5]}
        coloniasReturn.append(auxDetails)

    if len(coloniasReturn) > 0 :
        response = jsonify({"Colonias":coloniasReturn})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
    else:
        response = jsonify({"Messange": "not found", "status":404})
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response

if __name__ == '__main__':
    app.run(debug=True, port=5000)