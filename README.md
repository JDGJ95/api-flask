# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Instrucciones para el despliege del proyecto ###

* debemos tener la version de python 3.10
* debemos tener MySql

### primeros pasos ###

* lo primero que tenemos que realizar es crear una base de datos con el nombre de 'sepomex'

* teniendo creada la base de datos procedemos a crear las tablas con el siguiente script o lo podemos encontrar dentro del repositorio con el nombre de sepomex.sql
```bash
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for colonias
-- ----------------------------
DROP TABLE IF EXISTS `colonias`;
CREATE TABLE `colonias`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `d_codigo` int(11) NULL DEFAULT NULL,
  `d_asentamiento` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `d_cp` int(11) NULL DEFAULT NULL,
  `d_zona` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_municipio` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_munpio`(`id_municipio`) USING BTREE,
  CONSTRAINT `fk_munpio` FOREIGN KEY (`id_municipio`) REFERENCES `municipios` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 31814 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for estados
-- ----------------------------
DROP TABLE IF EXISTS `estados`;
CREATE TABLE `estados`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `d_estado` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `c_estado` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for municipios
-- ----------------------------
DROP TABLE IF EXISTS `municipios`;
CREATE TABLE `municipios`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `D_municipio` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `d_ciudad` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_estado` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_edo`(`id_estado`) USING BTREE,
  CONSTRAINT `fk_edo` FOREIGN KEY (`id_estado`) REFERENCES `estados` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2338 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
```
### configuracion ###

* Teniendo la base datos creada, ocuparemos las credenciales para realizar la conexion, para ello abrimos con algun editor de texto los archivos `app.py` y `seeder.py` que se encuentran de ntro de la carpeta app
* una vez dentro de los archivos pondremos el usuario y la contraseña para la conexion con mysql, en el archivo app.py de la siguiente manera
```bash
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'sepomex'
```

* en el archivo seeder.py sera de la siguiente manera con los datos de su usuario y contraseña

```bash
conexion = mysql.connector.connect( host='localhost', user= 'root', passwd='', db='sepomex' )
```
una vez puesto las credenciales correspondientes guardamos y salimos del editor de texto

### seeder y peticiones ###

* Teniendo las credenciales procederemos a instalar unos paketes necesarios para que nuestro proyecto corra adecuadamente

`pip install virtualenv`
`pip install flask`
`pip install mysqlclient`

* Teniendo los paquetes instalados procederemos a realizar el proceso de seeder, para ello dentro de nuestra terminal dentro del proyecto ingresamos a la carpeta app y ejecutamos el siguinte comando
`python seeder.py`
El proceso emezara a insertar toda la informacon, una vez acabado el proceso procederemos a activar nuestro servidor local que tiene cargado nuestra API con el siguiente comando
`python app.py`

ya que se este corriendo nuestro servidor local podremos realizar las siguintes peticiones de algun cliente como postman
```bash
* `http://127.0.0.1:5000/estados` #peticion tipo GET obtiene todos los estados
* `http://127.0.0.1:5000/estados/nombreEstado` #peticion tipo GET obtiene el estado por nombre
* `http://127.0.0.1:5000/new/estado?estado=nombreEstado&codigo=codigoEstado(int)` #peticion tipo POST inserta un nuevo estado

* `http://127.0.0.1:5000/municipios` #peticion tipo GET obtiene todos los municipio
* `http://127.0.0.1:5000/municipios/nombreEstado` #peticion tipo GET obtiene el municipio por nombre
* `http://127.0.0.1:5000/new/municipio?municipio=nombreMunicipio&codigo=CodigoMunicipio&idEdo=id del estado al que pertenece el municipio` #peticion tipo POST inserta un nuevo municipio

* `http://127.0.0.1:5000/colonias` #peticion tipo GET obtiene todos las colonias
* `http://127.0.0.1:5000/colonias/nombreEstado` p#eticion tipo GET obtiene la colonia por nombre
* `http://127.0.0.1:5000/colonias/cp` #peticion tipo GET obtiene la colonia por el codigo postal
* `http://127.0.0.1:5000/new/colonia?cp=codigoPostal&colonia=nombreColonia&zona=Rural o Urbana&idMnpio= id del minicipio que corresponde la colonia` #peticion tipo POST inserta una nueva colonia
```

### WebApp ###
* Dentro de nuetro repositorio encontraremos un archivo .html llamdo `webApp.html` el cual nos ayudara para realizar petciones al servidor, solo se ocupa abrir en algun navegador como chrome o fireFox